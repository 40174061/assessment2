﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

//created by Sean Ross
// Staff class used for all members of staff
//Last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class Staff
    {
        private string name; //staff members name
        private int staffID; //staff members ID number

        public string Name //accessor for staff name
        {
            get
            {
                return name;
            }
            set
            {
                if (value.Equals("")) //makes sure input is not null
                {
                    throw new ArgumentException("The staff member's name cannot be null"); //will be displayed if input is null
                }
                name = value;
            }
        }

        public int StaffID //accessor for staffID variable
        {
            get
            {
                return staffID;
            }
            set
            {
                if (value == 0) //checks if value is valid
                {
                    throw new ArgumentException("Please enter a value in the staff ID field that is not 0"); //will be display if value is 0 or no value entered
                }
                staffID = value;
            }
        }
        public void lookupStaffID(String staffName) //finds the members ID based on their name
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            String sql = String.Format(@"SELECT id FROM staff WHERE name='{0}'", staffName);//gets the staffID from the name selected
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                StaffID = (int)sdr["id"];//sets the staffID attribute equal to the result
            }
            sdr.Close();
        }
    }
}
