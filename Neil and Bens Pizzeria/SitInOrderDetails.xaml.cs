﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

//created by Sean Ross
//the details of a sit in order created
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    /// <summary>
    /// Interaction logic for SitInOrderDetails.xaml
    /// </summary>
    public partial class SitInOrderDetails : Window
    {
        SitInOrder thisOrder = new SitInOrder(); //creates a new instance of order
        public SitInOrderDetails()
        {
            InitializeComponent();
            loadServers(); //loads all servers stored in the database into the program
            loadDishes(); //loads all dishes stored in the database into the program
        }

        private void loadServers() //loads servers from the database
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand("SELECT name FROM Staff WHERE stafftype='Server'", con); //selects only the servers name from the database
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                this.comboServersBox.Items.Add(sdr["name"]); //adds the servers name to the combox box
            }
            sdr.Close();
        }
        private void loadDishes() //loads dishes from the database
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand("SELECT description FROM menuitems", con); //selects the dishes description
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                this.listBoxDishes.Items.Add(sdr["description"]); //adds the description to the list box
            }
            sdr.Close();
        }
        private void btnAddDish_Click(object sender, RoutedEventArgs e) //when the Add Dish button is clicked
        {
            if (this.listBoxDishes.SelectedItem == null) //checks to see if a dish has been selected
            {
                MessageBox.Show("Please select a dish"); //displayed if no dish is selected
            }
            else
            {
                String selectedDish = this.listBoxDishes.SelectedItem.ToString(); //gets the description of the selected dish
                this.listBoxOrder.Items.Add(selectedDish); //adds the selected dish to the listbox
                thisOrder.items.Add(selectedDish); //adds the dish to the order
            }
        }
        private void btnCreateBill_Click(object sender, RoutedEventArgs e) //when the Create Bill button is clicked
        {
            if (comboServersBox.SelectedItem == null) //checks if a server has been selected
            {
                MessageBox.Show("Please select a server"); //shown if no server selected
            }
            else if (comboTableBox.SelectedItem == null) //checks a table has been selected
            {
                MessageBox.Show("Please select a table"); //shown if no table selected
            }
            else if (listBoxOrder.Items.Count < 2) //checks that the order has items on it (set to 2 as the 'Your Order' message is also in the listbox)
            {
                MessageBox.Show("Please add at least 1 item to the order"); //shown if no items have been added to the order
            }
            else
            {
                Bill theBill = new Bill(); //creates a bill
                Server theServer = new Server(); //creates an instance of the server object
                theServer.Name = this.comboServersBox.Text; //sets the servers name
                thisOrder.Table = int.Parse(this.comboTableBox.Text); //sets the table number
                thisOrder.lookupOrderNumber(); //generates a new unique order number
                foreach (String item in thisOrder.items) //takes each item in the order 1 at a time
                {                    
                    MenuItem thisItem = new MenuItem();//creates a new instance of the menu item
                    thisItem.lookupItem(item); //looks up the item to get all of its details
                    theBill.txtbxBill.AppendText(thisItem.Description + "\t £" + thisItem.Price + "\n"); //adds the item to the viewable bill textbox
                    thisOrder.Total += thisItem.Price; //adds the items price to the total
                    thisOrder.lookupTransID();//gets a new unique transaction ID for each item on the order (give the database a clean unique primary key)
                    thisOrder.saveOrder(thisOrder.OrderNumber, thisItem.Description, theServer.Name, "SitIn", thisOrder.Table, "NA", thisItem.Price, thisOrder.TransactionID); //save the item in this order to the database
                }
                theBill.txtbxBill.AppendText("---------------------------------------" + "Total: \t £" + thisOrder.Total + "\n" + "Server: " + theServer.Name + "\t" + "Table: " + thisOrder.Table + "\n Order Number: " + thisOrder.OrderNumber); //totals the orders and displays all relevant details at the bottom
                theBill.ShowDialog(); //shows the bill in a new window
            }
        }
    }
}
