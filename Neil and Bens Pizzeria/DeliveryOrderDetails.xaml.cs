﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

//created by Sean Ross
//the details of a delivery order placed
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    /// <summary>
    /// Interaction logic for DeliveryOrderDetails.xaml
    /// </summary>
    public partial class DeliveryOrderDetails : Window
    {
        DeliveryOrder thisOrder = new DeliveryOrder();//new instance of delivery order
        public DeliveryOrderDetails()
        {
            InitializeComponent();
            loadDrivers();//loads drivers
            loadDishes();//loads menu items
        }
        private void loadDrivers()//loads drivers from database
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand("SELECT name FROM Staff WHERE stafftype='Driver'", con);//gets driver names from database
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                this.comboDriversBox.Items.Add(sdr["name"]);//adds names to combobox
            }
            sdr.Close();
        }
        private void loadDishes()//loads dishes from database
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand("SELECT description FROM menuitems", con);//gets item description
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                this.listBoxDishes.Items.Add(sdr["description"]);//adds menu items to list box
            }
            sdr.Close();
        }
        private void btnAddDish_Click(object sender, RoutedEventArgs e)//adds dish to the bill
        {
            if (this.listBoxDishes.SelectedItem == null)//checks a dish has been selected
            {
                MessageBox.Show("Please select a dish");//if no dish selected
            }
            else
            {
                String selectedDish = this.listBoxDishes.SelectedItem.ToString();//gets description of selected dish
                this.listBoxOrder.Items.Add(selectedDish);//adds dish to order listbox
                thisOrder.items.Add(selectedDish);//adds dish to order object
            }
        }
        private void btnCreateBill_Click(object sender, RoutedEventArgs e)//creates the bill
        {
            if (comboDriversBox.SelectedItem == null)//checks a driver has been selected
            {
                MessageBox.Show("Please select a server");//shows if no driver selected
            }
            else if (listBoxOrder.Items.Count < 2)//checks an item has been added to the order
            {
                MessageBox.Show("Please add at least 1 item to the order");//if no items added
            }
            else if (txtBxCustomerName.Text.ToString() == "")//checks a customer name has been given
            {
                MessageBox.Show("Please enter the customer name");//if no name given
            }
            else if (txtBxAddress.Text.ToString() == "")//checks address has been given
            {
                MessageBox.Show("Please enter the delivery address");//if no address given
            }
            else
            {
                Bill theBill = new Bill();//creates bill instance
                Driver theDriver = new Driver();//creates driver instance
                theDriver.Name = this.comboDriversBox.Text;//sets driver name
                theDriver.lookupStaffID(theDriver.Name);//looks up driver ID
                thisOrder.Customername = txtBxCustomerName.Text.ToString();//sets customer name
                thisOrder.DeliveryAddress = txtBxAddress.Text.ToString();//sets customer address
                thisOrder.lookupOrderNumber();//generates a new order number
                foreach (String item in thisOrder.items)//takes each item 1 at a time
                {
                    MenuItem thisItem = new MenuItem();//new instance of menu item
                    thisItem.lookupItem(item);//looks up menu item details
                    theBill.txtbxBill.AppendText(thisItem.Description + "\t £" + thisItem.Price + "\n");//adds item to the bill
                    thisOrder.Total += thisItem.Price;//adds item price to the total
                    thisOrder.lookupTransID();//generates unique transacation ID for each item
                    thisOrder.saveOrder(thisOrder.OrderNumber, thisItem.Description, theDriver.Name, "Delivery", 0, thisOrder.Customername, thisItem.Price, thisOrder.TransactionID);//saves item to the order
                }
                thisOrder.calculateDeliveryCharge();//calculates delivery charge
                Decimal Total = Math.Round(thisOrder.Total, 2);//rounds the total
                Decimal DeliveryCharge = Math.Round(thisOrder.DeliveryCharge, 2);//rounds the delivery charge
                theBill.txtbxBill.AppendText("Delivery Charge \t £" + DeliveryCharge + " ");//adds delivery charge to the bill
                theBill.txtbxBill.AppendText("---------------------------------------" + "Total: \t £" + Total + "\n" + "Customer Name: " + thisOrder.Customername + "\n Address: " + thisOrder.DeliveryAddress + " \n Driver: " + theDriver.Name + "\n Order Number: " + thisOrder.OrderNumber);//all other details for this order
                theBill.ShowDialog();
            }
        }

        private void btnCustomerDetails_Click(object sender, RoutedEventArgs e)//opens customer details grid
        {
            if (CustomerDetailsGrid.Visibility == Visibility.Hidden)//toggles visibility
            {
                CustomerDetailsGrid.Visibility = Visibility.Visible;
            }
            else CustomerDetailsGrid.Visibility = Visibility.Hidden;
        }
    }
}
