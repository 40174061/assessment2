﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//created by Sean Ross
//the main navigation window for the program
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }



        private void btnCreateSitIn_Click(object sender, RoutedEventArgs e)//button creates a sit in order
        {
            SitInOrderDetails detailsWindow = new SitInOrderDetails();//new instance of sit in order window
            detailsWindow.ShowDialog();//dialog so only order window can be edited
        }

        private void btnCreateDelivery_Click(object sender, RoutedEventArgs e)//button to create delivery order
        {
            DeliveryOrderDetails detailsWindow = new DeliveryOrderDetails();//creates new instance of details window
            detailsWindow.ShowDialog();
        }

        private void btnManagementFunctions_Click(object sender, RoutedEventArgs e)//button to show management functions
        {
            ManagementFunctions managementFunctions = new ManagementFunctions();//new instance of management functions window
            managementFunctions.ShowDialog();
        }
    }
}
