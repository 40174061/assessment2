﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

//created by Sean Ross
//Methods for controlling the database to be operated by a manager
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    /// <summary>
    /// Interaction logic for ManagementFunctions.xaml
    /// </summary>
    public partial class ManagementFunctions : Window
    {
        public ManagementFunctions()
        {
            InitializeComponent();
        }
        private String database; //determines which database to lookup
        private String currentDescription; //saves the current description for the update SQL statement
        private void btnDisplayMenuItems_Click(object sender, RoutedEventArgs e)//shows all menu items in the listbox
        {
            database = "MenuItems"; //sets the database to the menuitems table
            btnAddMenuItem.Visibility = Visibility.Visible; //displays the add control for this table
            btnUpdateMenuItem.Visibility = Visibility.Visible; //displays the update control for this table
            btnDeleteMenuItem.Visibility = Visibility.Visible; //displays the delete control for this table
            btnAddStaff.Visibility = Visibility.Hidden; //hides the add staff button
            btnUpdateStaff.Visibility = Visibility.Hidden;//hides the update staff button
            btnDeleteStaff.Visibility = Visibility.Hidden;//hides the delete staff button
            btnFilterItem.Visibility = Visibility.Hidden;//hides the fitler orders button
            btnFilterServer.Visibility = Visibility.Hidden;//hides the filter orders button
            listBoxItems.Items.Clear();//clear the listbox before adding any values, eliminates the possibility of values repeating or unintended values
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand(@"SELECT description FROM MenuItems", con); //selects item name from the menu items table
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                listBoxItems.Items.Add(sdr["description"]);//shows the items name in the list box
            }
            sdr.Close();
        }

        private void btnDisplayStaff_Click(object sender, RoutedEventArgs e) //shows all staff members in the listbox
        {
            database = "Staff"; //sets the database to the staff table
            btnAddMenuItem.Visibility = Visibility.Hidden; //hides the add menu item button
            btnUpdateMenuItem.Visibility = Visibility.Hidden; //hides the update menu item button
            btnDeleteMenuItem.Visibility = Visibility.Hidden; //hides the delete menu item button
            btnAddStaff.Visibility = Visibility.Visible; //shows the add control for this table
            btnUpdateStaff.Visibility = Visibility.Visible;//shows the update control for this table
            btnDeleteStaff.Visibility = Visibility.Visible;//shows the delte control for this table
            btnFilterItem.Visibility = Visibility.Hidden;//hides the filter control for order
            btnFilterServer.Visibility = Visibility.Hidden;//hides the filter control for orders
            listBoxItems.Items.Clear();//clears the listbox to show only these values
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand(@"SELECT name FROM Staff", con);//selects the staff members name
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                listBoxItems.Items.Add(sdr["name"]);//shows the staff members name in the list box
            }
            sdr.Close();
        }

        private void btnDisplayOrders_Click(object sender, RoutedEventArgs e)//displays all orders taken in the listobx
        {
            database = "Orders"; //sets the database to the orders table
            btnAddMenuItem.Visibility = Visibility.Hidden; //hides the add menu item button
            btnUpdateMenuItem.Visibility = Visibility.Hidden;//hides the update menu item button
            btnDeleteMenuItem.Visibility = Visibility.Hidden;//hides the delete menu item button
            btnAddStaff.Visibility = Visibility.Hidden;//hides the add staff button
            btnUpdateStaff.Visibility = Visibility.Hidden;//hides the update staff button
            btnDeleteStaff.Visibility = Visibility.Hidden;//hides the delete staff button
            btnFilterItem.Visibility = Visibility.Visible;//shows the item filter button
            btnFilterServer.Visibility = Visibility.Visible;//shows the staff filter button
            listBoxItems.Items.Clear();//clears the listbox to show only these values
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand com = new SqlCommand(@"SELECT OrderID FROM Orders WHERE OrderID > 0 GROUP BY orderID", con);//selects each order ID from the databse
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                listBoxItems.Items.Add(sdr["OrderID"]);//shows the order ID in the listbox
            }
            sdr.Close();
        }
        private void listBoxItems_SelectionChanged(object sender, SelectionChangedEventArgs e)//triggered every time the selected item in the list box is changed
        {
            listBoxDetails.Items.Clear(); //clears the details box
            if (listBoxItems.SelectedItem == null)//if nothing has been selected in the list box
            {
                listBoxItems.Items.Clear();//clears the list box
            }
            else
            {
                String selectedItem = listBoxItems.SelectedItem.ToString(); //saves the selected items description
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                if (database.Equals("MenuItems"))//for searching the menuItems table
                {
                    String sql = String.Format(@"SELECT * FROM {0} WHERE description='{1}'", database, selectedItem);//gets all details of the selected menu item
                    SqlCommand com = new SqlCommand(sql, con);
                    SqlDataReader sdr = com.ExecuteReader();
                    while (sdr.Read())
                    {
                        listBoxDetails.Items.Add("Description: \n" + sdr["description"]);//adds the description to the list box
                        listBoxDetails.Items.Add("Vegetarian: \n" + sdr["vegetarian"]);//adds the vegetarian boolean to the listbox
                        listBoxDetails.Items.Add("Price: \n £" + sdr["price"]);//adds the items price to the listbox
                    }
                    sdr.Close();
                }      
                else if (database.Equals("Staff"))//for searching the staff table
                {
                    String sql = String.Format(@"SELECT * FROM {0} WHERE name='{1}'", database, selectedItem);//gets all staff details for selected staff member
                    SqlCommand com = new SqlCommand(sql, con);
                    SqlDataReader sdr = com.ExecuteReader();
                    while (sdr.Read())
                    {
                        listBoxDetails.Items.Add("Name: \n" + sdr["Name"]);//adds the staff members name to the details box
                        listBoxDetails.Items.Add("ID: \n" + sdr["ID"]);//adds the staff members ID to the datils box
                        listBoxDetails.Items.Add("Staff Type: \n" + sdr["StaffType"]);//adds the staff type to the details box
                        listBoxDetails.Items.Add("Car Reg: \n" + sdr["CarReg"]);//adds the staff members carreg to the details box, some staff members are not required to provide a car reg
                    }
                    sdr.Close();
                }      
                else if (database.Equals("Orders"))//searches the orders table
                {
                    int x = 0;//temporary variable for adding items to the details box
                    Decimal Total = 0; //the total bill value
                    Boolean Delivery = false; //if order was a delivery
                    String sql = String.Format(@"SELECT * FROM {0} WHERE OrderID='{1}'", database, selectedItem);//gets all the order details for the selected order
                    SqlCommand com = new SqlCommand(sql, con);
                    SqlDataReader sdr = com.ExecuteReader();                    
                    while (sdr.Read())
                    {   
                        if(x == 0)
                        {
                            listBoxDetails.Items.Add("Order Number: \n" + sdr["OrderID"]); //adds the order number to the details window
                            listBoxDetails.Items.Add("Staff Member: \n" + sdr["StaffName"]);//adds the staff member to the details window

                            if (sdr["OrderType"].Equals("SitIn"))//checks the order type for sit in orders
                            {
                                Delivery = false; //order is not delivery
                                listBoxDetails.Items.Add("Order Type: \n Sit In");//adds that the order is sit in to the details window
                                listBoxDetails.Items.Add("Table: \n" + sdr["Table"]); //adds the table number to the details window
                            }          
                            else if (sdr["OrderType"].Equals("Delivery"))//checks if order was a delivery
                            {
                                Delivery = true;//sets ordertype to delivery
                                listBoxDetails.Items.Add("Order Type: \n Delivery");//adds order type to details window
                                listBoxDetails.Items.Add("Customer Name: \n" + sdr["CustomerName"]);//adds the customers name to the details window
                            }            
                            listBoxDetails.Items.Add("Items:"); //adds the items header
                        }                        
                        listBoxDetails.Items.Add(sdr["Item"] + "\t £" + sdr["ItemAmount"]);//adds each item under the item header
                        x += 1;//used to now only select item details rather than repeat the entire order details
                        Total += Decimal.Parse(sdr["ItemAmount"].ToString());//adds the item amount to the total
                    }
                    if (Delivery == true)//checks delivery orders
                    {
                        Decimal DeliveryCharge = Math.Round(Decimal.Multiply(Total, 0.15m), 2);//calculates the delivery charge
                        listBoxDetails.Items.Add("Delivery Charge \t £" + DeliveryCharge);//shows the delivery charge
                        Total += DeliveryCharge;//calculates the total
                    }
                    listBoxDetails.Items.Add("Total: \t £" + Total);//shows the total
                    sdr.Close();
                    x = 0;//resets the temporary variable for use the next time
                }
            }
        }

        private void btnFilterItem_Click(object sender, RoutedEventArgs e)//filter the orders by items ordered and show the quantity
        {
            listBoxDetails.Items.Clear();
            listBoxItems.Items.Clear();
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            String sql = String.Format(@"SELECT Item, COUNT(Item) AS totalItems FROM Orders WHERE Item NOT IN ('NA') GROUP BY Item");//select the item and how many sold
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                listBoxDetails.Items.Add("Item: " + sdr["Item"] + "\n" + "Qty Sold: " + sdr["totalItems"]); //shows the item and how many have sold
            }
            sdr.Close();
        }

        private void btnFilterServer_Click(object sender, RoutedEventArgs e)//filter the orders by which staff member dealt with them
        {
            listBoxSelectServer.Items.Clear();//clears the listbox
            listBoxDetails.Items.Clear();//clears the list box
            listBoxItems.Items.Clear();//clears the list box
            if (listBoxSelectServer.Visibility == Visibility.Hidden)//toggles the vosibility of the server selection box
            {
                listBoxSelectServer.Visibility = Visibility.Visible;//shows the server selection box
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                String sql = String.Format(@"SELECT Name FROM Staff");//selcts the staff names from the staff table
                SqlCommand com = new SqlCommand(sql, con);
                SqlDataReader sdr = com.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxSelectServer.Items.Add(sdr["Name"]);//adds all staff names to the selection box
                }
                sdr.Close();

            }
            else if (listBoxSelectServer.Visibility == Visibility.Visible)//toggles visibility of the selection box
            {
                listBoxSelectServer.Visibility = Visibility.Hidden;//hides the selection box
            }
        }

        private void listBoxSelectServer_SelectionChanged(object sender, SelectionChangedEventArgs e)//if a server has been selected or changed
        {
            if (listBoxSelectServer.SelectedItem == null)//safety check stops program from crashing when toggling the selection box
            {
                listBoxSelectServer.Items.Clear();//clears the selection box
            }
            else
            {
                String Server = listBoxSelectServer.SelectedItem.ToString();//saves the servers name
                listBoxSelectServer.Visibility = Visibility.Hidden;//hides the selection box
                listBoxItems.Items.Clear();//clears the list box
                listBoxDetails.Items.Clear();//clears the details box
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                String sql = String.Format(@"SELECT OrderID FROM Orders WHERE StaffName='{0}' GROUP BY OrderID", Server);//selects all orders from the specified server
                SqlCommand com = new SqlCommand(sql, con);
                SqlDataReader sdr = com.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["orderID"]);//adds each order ID to the list box
                }
                sdr.Close();
            }            
        }

        private void btnDeleteMenuItem_Click(object sender, RoutedEventArgs e)//deletes a menu item from the database
        {
            if (listBoxItems.SelectedItem == null)//checks an item has been selected
            {
                MessageBox.Show("Please select and item to delete");//if no item has been selected this message will show
            }
            else
            {
                String menuItem = listBoxItems.SelectedItem.ToString();//sves the menu items description
                String sql = String.Format(@"DELETE FROM MenuItems WHERE Description='{0}'", menuItem);//deletes the selected menu item from the database
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand deleteItem = new SqlCommand(sql, con);
                deleteItem.ExecuteNonQuery();
                listBoxItems.Items.Clear();//clears the list box of all values including the deleted value
                SqlCommand UpdateItems = new SqlCommand(@"SELECT Description FROM MenuItems", con);//updates the items in the listbox to show this change
                SqlDataReader sdr = UpdateItems.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["Description"]);//adds each item to the list box again
                }
                sdr.Close();
            }            
        }

        private void btnUpdateMenuItem_Click(object sender, RoutedEventArgs e)//update a menu items details
        {
            if (listBoxItems.SelectedItem == null)//checks if an item has been selected
            {
                MessageBox.Show("Please select an item to update");//shown if no item selected
            }       
            else
            {
                MainMenu.Visibility = Visibility.Hidden;//hides the main controls
                GridAddMenuItem.Visibility = Visibility.Visible;//shows controls for menu items
                btnUpdateItem.Visibility = Visibility.Visible;//shows the update button
                btnAddItem.Visibility = Visibility.Hidden;//hides the add button
                String menuItem = listBoxItems.SelectedItem.ToString();//saves the menu items decription
                String sql = String.Format(@"SELECT * FROM MenuItems WHERE Description='{0}'", menuItem);
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand lookupItems = new SqlCommand(sql, con);
                SqlDataReader sdr = lookupItems.ExecuteReader();
                while (sdr.Read())
                {
                    txtBxDescription.Text = sdr["Description"].ToString();//shows the items description
                    txtBxPrice.Text = sdr["Price"].ToString();//shows the items price
                    if (sdr["Vegetarian"].ToString().Equals("Y"))//shows whether or not the item is vegetarian
                    {
                        radioButtonYes.IsChecked = true;
                    }
                    else radioButtonNo.IsChecked = true;
                }
                currentDescription = txtBxDescription.Text;//sets the current description of the item in case it is changed, used for updating the table
                sdr.Close();
            }            
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)//cancel the current operation
        {
            txtBxDescription.Clear();//clears the decription box
            txtBxPrice.Clear();//clears the price
            GridAddMenuItem.Visibility = Visibility.Hidden;//hides controls for mnu items
            MainMenu.Visibility = Visibility.Visible;//shows all main controls
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)//adds an item to the database
        {
            if (txtBxDescription.Text.Equals(""))//makes sure the box has a value
            {
                MessageBox.Show("Please enter an item description"); //if box has no value
            }
            else if (txtBxPrice.Text.Equals(""))//makes sure price has a value
            {
                MessageBox.Show("Please enter a price for the item");//if no price given
            }
            else
            {
                MenuItem thisItem = new MenuItem();//creates an instacne of the menu item
                thisItem.Description = txtBxDescription.Text; //sets item description
                thisItem.Price = Decimal.Parse(txtBxPrice.Text);//sets item price
                String Vegetarian = "N";//default value for vegetarian
                if (radioButtonYes.IsChecked == true)
                {
                    Vegetarian = "Y";//converts to value compatible ith database
                }
                else if (radioButtonNo.IsChecked == true)
                {
                    Vegetarian = "N";//converts to value compatible with database
                }
                String sql = String.Format(@"INSERT INTO MenuItems VALUES ('{0}', {1}, '{2}')", thisItem.Description, thisItem.Price, Vegetarian); //inserts the new menu item into the database
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand addItem = new SqlCommand(sql, con);
                addItem.ExecuteNonQuery();
                txtBxDescription.Clear();//clears the decription box
                txtBxPrice.Clear();//clears the price box
                GridAddMenuItem.Visibility = Visibility.Hidden;//hides the mneu item controls
                MainMenu.Visibility = Visibility.Visible;//shows all main controls
                listBoxItems.Items.Clear();//clears the listbox so it can be updated with the current menu items
                SqlCommand UpdateItems = new SqlCommand(@"SELECT Description FROM MenuItems", con);//selects all menu items from the database including new item
                SqlDataReader sdr = UpdateItems.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["Description"]);//shows each item description in the list box
                }
                sdr.Close();
            }
        }

        private void btnAddMenuItem_Click(object sender, RoutedEventArgs e)//shows controls for adding a menu item
        {
            MainMenu.Visibility = Visibility.Hidden;//hides main controls
            GridAddMenuItem.Visibility = Visibility.Visible;//shows menu item controls
            btnUpdateItem.Visibility = Visibility.Hidden;//hides update button
            btnAddItem.Visibility = Visibility.Visible;//shows add item button
        }

        private void btnUpdateItem_Click(object sender, RoutedEventArgs e)//updates a selected menu item
        {
            if (txtBxDescription.Text.Equals(""))//checks item has description
            {
                MessageBox.Show("Please enter an item description");//if item has no description
            }
            else if (txtBxPrice.Text.Equals(""))//checks item has price
            {
                MessageBox.Show("Please enter a price for the item");//if item has no price
            }
            else
            {
                MenuItem thisItem = new MenuItem();//creates instance of a menu item
                thisItem.Description = txtBxDescription.Text;//saves item description
                thisItem.Price = Decimal.Parse(txtBxPrice.Text);//saves item price
                String Vegetarian = "N";//default value for vegetarian
                if (radioButtonYes.IsChecked == true)
                {
                    Vegetarian = "Y";//value compatible with database
                }
                else if (radioButtonNo.IsChecked == true)
                {
                    Vegetarian = "N";//value compatible with database
                }
                String sql = String.Format(@"UPDATE MenuItems SET Description='{0}', Price={1}, Vegetarian='{2}' WHERE Description='{3}'", thisItem.Description, thisItem.Price, Vegetarian, currentDescription);//updates the menu item with the current details
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand updateItem = new SqlCommand(sql, con);
                updateItem.ExecuteNonQuery();
                txtBxDescription.Clear();//clears the description box
                txtBxPrice.Clear();//clears the price box
                GridAddMenuItem.Visibility = Visibility.Hidden;//hides menu items controls
                MainMenu.Visibility = Visibility.Visible;//shows main controls
                listBoxItems.Items.Clear();//clears the list box
                SqlCommand UpdateItems = new SqlCommand(@"SELECT Description FROM MenuItems", con);//gets all menu items including updated item
                SqlDataReader sdr = UpdateItems.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["Description"]);//adds item description to list box
                }
                sdr.Close();
            }
        }

        private void btnCancel1_Click(object sender, RoutedEventArgs e)//cancel button for staff members
        {
            txtBxName.Clear();//clears the name box
            txtBxID.Clear();//clears the id box
            txtBxCarReg.Clear();//clears the car reg box
            GridStaffDetails.Visibility = Visibility.Hidden;//hides staff controls
            MainMenu.Visibility = Visibility.Visible;//shows main controls
        }

        private void btnAddStaff_Click(object sender, RoutedEventArgs e)//shows details for adding a staff member
        {
            MainMenu.Visibility = Visibility.Hidden;//hides main controls
            GridStaffDetails.Visibility = Visibility.Visible;//shows staff controls
            btnAddStaffMember.Visibility = Visibility.Visible;//shows add staff button
            btnUpdateStaffMember.Visibility = Visibility.Hidden;//hides update staff button
            String sql = String.Format(@"SELECT MAX(ID) as maxID FROM Staff");//gets the current max value of staff ID
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand generateStaffID = new SqlCommand(sql, con);
            SqlDataReader sdr = generateStaffID.ExecuteReader();
            while (sdr.Read())
            {
                int newStaffID = (int)sdr["maxID"] + 1; //generates a new staff ID number 1 gearter thanthe previous max value;
                txtBxID.Text = newStaffID.ToString();//shows this ID in the ID box
            }
        }

        private void radioButtonDriver_Checked(object sender, RoutedEventArgs e)//toggles ability to control car reg box
        {
            txtBxCarReg.IsEnabled = true;
        }

        private void radioButtonDriver_Unchecked(object sender, RoutedEventArgs e)//toggles ability to control car reg box
        {
            txtBxCarReg.IsEnabled = false;
        }

        private void btnUpdateStaff_Click(object sender, RoutedEventArgs e)//shows details for updating staff details
        {
            if (listBoxItems.SelectedItem == null)//checks a staff member has been selected 
            {
                MessageBox.Show("Please select an item to update");//shown if nothing selected
            }
            else
            {
                MainMenu.Visibility = Visibility.Hidden;//hides main controls
                GridStaffDetails.Visibility = Visibility.Visible;//shows staff controls
                btnUpdateStaffMember.Visibility = Visibility.Visible;//shows update staff button
                btnAddStaffMember.Visibility = Visibility.Hidden;//hides add staff button
                String staffMember = listBoxItems.SelectedItem.ToString();//saves staff name
                String sql = String.Format(@"SELECT * FROM Staff WHERE Name='{0}'", staffMember);//gets the staff members current details
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand lookupStaff = new SqlCommand(sql, con);
                SqlDataReader sdr = lookupStaff.ExecuteReader();
                while (sdr.Read())
                {
                    txtBxName.Text = sdr["Name"].ToString();//sets the name box to the current value
                    txtBxID.Text = sdr["ID"].ToString();//sets the ID to this staff members ID (this cannot be changed)
                    if (sdr["StaffType"].ToString().Equals("Server"))//checks staff type
                    {
                        radioButtonServer.IsChecked = true;
                    }
                    else if (sdr["StaffType"].ToString().Equals("Driver"))//checks staff type
                    {
                        radioButtonDriver.IsChecked = true;
                    }
                    txtBxCarReg.Text = sdr["CarReg"].ToString();//sets the car reg to the current value, only drivers have to supply a car reg
                }
                sdr.Close();
            }
        }

        private void btnDeleteStaff_Click(object sender, RoutedEventArgs e)//deletes a staff member from the database
        {
            if (listBoxItems.SelectedItem == null)//cehcks an item has been selected
            {
                MessageBox.Show("Please select and item to delete");//shows if no item selected
            }
            else
            {
                String name = listBoxItems.SelectedItem.ToString();//gets the name selected
                String sql = String.Format(@"DELETE FROM Staff WHERE name='{0}'", name);//deletes the selected item from the table
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand deleteStaff = new SqlCommand(sql, con);
                deleteStaff.ExecuteNonQuery();
                listBoxItems.Items.Clear();//clears the list box
                SqlCommand UpdateStaff = new SqlCommand(@"SELECT name FROM Staff", con);//loads current staff into list
                SqlDataReader sdr = UpdateStaff.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["Name"]);//loads names into the list
                }
                sdr.Close();
            }
        }

        private void btnAddStaffMember_Click(object sender, RoutedEventArgs e)//adds a staff member to the database
        {
            if (txtBxName.Text.Equals(""))//checks a name has been inserted
            {
                MessageBox.Show("Please enter a name for this staff member");//shows if no name has been entered
            }
            else if (radioButtonDriver.IsChecked == true && txtBxCarReg.Text.Equals(""))//checks if the staff member has been set as a driver and if they have a reg number added
            {
                MessageBox.Show("If this staff member is a driver, please enter a Car Registration Number, Otherwise select 'Server'");//shows if member is a driver but no reg number added
            }
            else
            {
                Staff staffMember = new Staff();//new instance of staff member
                staffMember.Name = txtBxName.Text;//name of staff member
                staffMember.StaffID = int.Parse(txtBxID.Text);//sets staff ID
                String sql;//the sql command string
                String StaffType;//the stafftype string
                if (radioButtonServer.IsChecked == true)//if staff is server
                {
                    StaffType = "Server";
                    sql = String.Format(@"INSERT INTO Staff (Name, ID, StaffType) VALUES ('{0}', {1}, '{2}')", staffMember.Name, staffMember.StaffID, StaffType);//insert server into table
                }
                else//if staff is driver
                {
                    StaffType = "Driver";
                    String CarReg = txtBxCarReg.Text;//car reg of the driver
                    sql = String.Format(@"INSERT INTO Staff (Name, ID, StaffType, CarReg) VALUES ('{0}', {1}, '{2}', '{3}')", staffMember.Name, staffMember.StaffID, StaffType, CarReg);//adds driver tothe table
                }
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand addStaff = new SqlCommand(sql, con);
                addStaff.ExecuteNonQuery();
                txtBxName.Clear();//clears the name box
                txtBxID.Clear();//clears the id box
                txtBxCarReg.Clear();//clears the car reg box
                MainMenu.Visibility = Visibility.Visible;//shows main controls
                GridStaffDetails.Visibility = Visibility.Hidden;//hides staff controls
                listBoxItems.Items.Clear();//clears list box
                SqlCommand UpdateStaff = new SqlCommand(@"SELECT name FROM Staff", con);//updates list box with current values
                SqlDataReader sdr = UpdateStaff.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["Name"]);//shows names of all staff members
                }
                sdr.Close();
            }
        }

        private void btnUpdateStaffMember_Click(object sender, RoutedEventArgs e)//updates staff member details
        {
            if (txtBxName.Text.Equals(""))//checks a name has been entered
            {
                MessageBox.Show("Please enter a name for this staff member");//shown if no name entered
            }
            else if (radioButtonDriver.IsChecked == true && txtBxCarReg.Text.Equals(""))//if staff is driver they must have a car reg
            {
                MessageBox.Show("If this staff member is a driver, please enter a Car Registration Number, Otherwise select 'Server'");//shown if driver has no car reg
            }
            else
            {
                Staff staffMember = new Staff();//new instance of staff member
                staffMember.Name = txtBxName.Text;//sets name of staff member
                staffMember.StaffID = int.Parse(txtBxID.Text);//sets ID for staff member
                String sql;//sql command
                String StaffType;//string for stafftype
                if (radioButtonServer.IsChecked == true)//code for servers
                {
                    StaffType = "Server";
                    sql = String.Format(@"UPDATE Staff SET Name='{0}', StaffType='{1}' WHERE ID={2}", staffMember.Name, StaffType, staffMember.StaffID);//updates if staff type is server
                }
                else//code for driver
                {
                    StaffType = "Driver";
                    String CarReg = txtBxCarReg.Text;//sets driver car reg
                    sql = String.Format(@"Update Staff SET Name='{0}', StaffType='{1}', CarReg='{2}' WHERE ID={3}", staffMember.Name, StaffType, CarReg, staffMember.StaffID);//updates if staff type is driver
                }
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
                con.Open();
                SqlCommand addStaff = new SqlCommand(sql, con);
                addStaff.ExecuteNonQuery();
                txtBxName.Clear();//clears name box
                txtBxID.Clear();//clears id box
                txtBxCarReg.Clear();//clears car reg box
                MainMenu.Visibility = Visibility.Visible;//shows main controls
                GridStaffDetails.Visibility = Visibility.Hidden;//hides staff controls
                listBoxItems.Items.Clear();//clears list box
                SqlCommand UpdateStaff = new SqlCommand(@"SELECT name FROM Staff", con);//populates staff box with current staff
                SqlDataReader sdr = UpdateStaff.ExecuteReader();
                while (sdr.Read())
                {
                    listBoxItems.Items.Add(sdr["Name"]);//shows name of staff members
                }
                sdr.Close();
            }
        }
    }
}
