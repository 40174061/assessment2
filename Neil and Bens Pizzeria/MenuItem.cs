﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

//created by Sean Ross
//Creates a menu item
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class MenuItem
    {
        private String description; //items description
        private bool vegetarian; //if itemis suitable for vegetarians
        private Decimal price; //items price in £

        public String Description //accessor for items description
        {
            get
            {
                return description;
            }
            set
            {
                if (value.Equals("")) //not valid if field is blank
                {
                    throw new ArgumentException("Item description cannot be blank");//displayed if value is blank
                }
                description = value;
            }
        }

        public bool Vegetarian //accessor for vegetarion boolean
        {
            get
            {
                return vegetarian;
            }
            set
            {
                vegetarian = value;
            }
        }

        public Decimal Price //accessor for items price
        {
            get
            {
                return price;
            }
            set
            {
                if (value < 0 || value > 1000) //value cannot be less than 0 or more than £1000
                {
                    throw new ArgumentException("Items must have a price between 0 and 100,000p (£100)");//displayed if value is not valid
                }
                price = value;
            }
        }
        public void lookupItem(String item)//looks up the items details based on its name which is unique
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            String sql = String.Format(@"SELECT description,price,vegetarian FROM menuitems WHERE description='{0}'", item);//pulls all details from the database
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                Description = (String)sdr["description"];
                Price = (Decimal)sdr["price"];
                String veg = (String)sdr["vegetarian"]; //temporary variable used for converting table character to boolean value
                if (veg.Equals("Y"))//coverts the databse string to a boolean value
                {
                    Vegetarian = true;
                }
                else Vegetarian = false;
            }
            sdr.Close();
        }
    }
}
