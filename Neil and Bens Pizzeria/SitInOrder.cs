﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

//created by Sean Ross
//creates a sit-in order with a table number and inhereted from orders
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class SitInOrder : Order //inheret from order class
    {
        private int table; //the table number for an order to be assigned to

        public int Table
        {
            get
            {
                return table;
            }
            set
            {
                if (value < 1 || value > 10)//table number must be between 1 and 10
                {
                    throw new ArgumentException("Table number must be between 1 and 10");//displayed if value is invalid
                }
                table = value;
            }
        }
    }
}
