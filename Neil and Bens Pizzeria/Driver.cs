﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//created by Sean Ross
//Driver class which is inhereted from staff class
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class Driver : Staff //inhereted from staff class
    {
        private string carRegistration; //the drivers car registration

        public string CarRegistration //accessor for drivers carRegistration
        {
            get
            {
                return carRegistration;
            }
            set
            {
                if (value.Equals("")) //makes sure a value is entered for car registration
                {
                    throw new ArgumentException("please set the drivers Car Registration"); //displayed if field is blank
                }
                carRegistration = value;
            }
        }
    }
}
