﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

//created by Sean Ross
//parent class used for taking orders
//Last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class Order
    {
        private int orderNumber; //a unique order number
        private int transactionID; //unique transaction ID for better database performance, a unique ID is given to every item added to a single order
        private Decimal total = 0m; //the total price of the order
        public List<String> items = new List<String>(); //creates a list of dishes ordered

        public int OrderNumber //accessor for order number
        {
            get
            {
                return orderNumber;
            }
            set
            {
                orderNumber = value;
            }
        }
        public int TransactionID //accessor for transaction ID
        {
            get
            {
                return transactionID;
            }
            set
            {
                transactionID = value;
            }
        }
        public Decimal Total //accessor for the total value
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }
        public void lookupOrderNumber()//gets the max order number that has already been assigned so we can generate the next number in the sequence
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            String sql = String.Format(@"SELECT MAX(orderid) AS ordernumber FROM orders");//gets the max order number already assigned
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                OrderNumber = (int)sdr["ordernumber"] + 1;//sets the current order number as the next one in the sequence
            }        
            sdr.Close();
        }
        public void lookupTransID()
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            String sql = String.Format(@"SELECT MAX(transid) AS maxtransid FROM orders");//gets the max tansaction number already assigned
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                TransactionID = (int)sdr["maxtransid"] + 1;//sets the current transaction ID as the next in the sequence
            }
            sdr.Close();
        }
        public void saveOrder(int orderID, String item, String StaffName, String OrderType, int Table, String CustomerName, Decimal Amount, int transID)//saves the order to the database
        {
            String insertOrder = String.Format(@"INSERT INTO orders VALUES ({0},'{1}','{2}','{3}',{4},'{5}',{6},{7})", orderID, item, StaffName, OrderType, Table, CustomerName, Amount, transID); //saves each item in an order to the database, takes in all details
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            SqlCommand addCom = new SqlCommand(insertOrder, con);    
            addCom.ExecuteNonQuery();//executes the sql command
        }
    }
}
