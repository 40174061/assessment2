﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

//created by Sean Ross
//Creates a server class inhereted from the staff class
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class Server : Staff //class is inhereted from staff
    {
        public void lookupServer(String name) //looks up the server to get their ID number
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\PizzeriaDatabase.mdf;Integrated Security=True");
            con.Open();
            String sql = String.Format(@"SELECT id, name FROM staff WHERE stafftype='Server' AND name='{0}'", name); //takes in the server name to return their ID
            SqlCommand com = new SqlCommand(sql, con);
            SqlDataReader sdr = com.ExecuteReader();
            while (sdr.Read())
            {
                Name = (String)sdr["name"];
                StaffID = (int)sdr["id"];
            }
            sdr.Close();
        }
    }
}
