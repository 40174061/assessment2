﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//created by Sean Ross
//Creates delivery order with customers delivery details
//last modified 09/12/15

namespace Neil_and_Bens_Pizzeria
{
    class DeliveryOrder : Order //inheret from order class
    {
        private string customerName; //the customers name;
        private string deliveryAddress; //the customers address
        public decimal deliveryChargePercentage = 0.15m; //the percentage at which the delivery charge is calculated
        private decimal deliveryCharge; //the actual delivery charge in monetary terms

        public string Customername //accessor for the customer name property
        {
            get
            {
                return customerName;
            }
            set
            {
                if (value.Equals("")) //checks if input is blank
                {
                    throw new ArgumentException("Please enter the customer's name"); //displayed if invalid
                }
                customerName = value;
            }
        }

        public string DeliveryAddress //accessor for delivery address
        {
            get
            {
                return deliveryAddress;
            }
            set
            {
                if (value.Equals("")) //checks if input is blank
                {
                    throw new ArgumentException("Please enter a delivery address"); //displayed if invalid
                }
                deliveryAddress = value;
            }
        }
        public decimal DeliveryCharge //accessor for delivery charge
        {
            get
            {
                return deliveryCharge;
            }
            set
            {
                deliveryCharge = value;
            }
        }
        public void calculateDeliveryCharge() //calculates the delivery charge
        {
            DeliveryCharge = decimal.Multiply(Total, deliveryChargePercentage); //calculates the delivery charge
            decimal.Round(DeliveryCharge, 2);//round the delivery charge
            Total += DeliveryCharge; //adds charge to total
        }
    }
}
